package TestNgMaven.TestNgMaven;

import org.testng.annotations.Test;

import TestNgMaven.TestNgMaven.XLS;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

import org.testng.annotations.Test;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;

public class POST {
	
	String path=System.getProperty("user.dir")+"/src/PUT&POST.xlsx";
	
  @Test(dataProvider = "dp")
  public void f(String ID  , String IDBook ,String Url) {
	  
	  
	   	
			
			RestAssured.baseURI="https://fakerestapi.azurewebsites.net/api/CoverPhotos";
				
			RequestSpecification httpRequest=RestAssured.given();
			//send POST Request
			JSONObject requestparms=new JSONObject();
			
			requestparms.put("id", ID);
			requestparms.put("idBook", IDBook);
			requestparms.put("url", Url);
			
			
			httpRequest.header("content-type","application/json");
			httpRequest.body(requestparms.toJSONString());
			
			Response response=httpRequest.request(Method.POST,"/create");
			
			String responseBody=response.getBody().asString();
			
			
			System.out.println("Response Body :-"+responseBody);
			
			Assert.assertEquals( responseBody.contains(ID),true);
			
			Assert.assertEquals( responseBody.contains(IDBook),true);
			Assert.assertEquals( responseBody.contains(Url),true);
			
			int statusCode=response.getStatusCode();
			Assert.assertEquals(statusCode,200);
			
	  
	  
	  
	  
	  
  }

  @DataProvider(name="dp")
  
  public Object[][] dp() throws IOException {
	  
	  
	  int rownum=XLS.getRowCount(path, "sheet1");
		int colcount=XLS.getCellCount(path, "sheet1", 1);
			
		
		 Object  coverData[][]= new Object[rownum][colcount];
		
		for(int i=1;i<=rownum;i++) {
			
			for(int j=0;j<=colcount;j++) {
				
				coverData[i-1][j]=XLS.getCellData(path, "sheet1", i, j);
				
			}
			
			
		}
		
		
		return  coverData;
	  
  }}
		  
		 
				
			
		  
		  
	
    
  
	  
  

