package TestNgMaven.TestNgMaven;

import static io.restassured.RestAssured.get;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class DELETE {
	
	
	
	@Test(dataProvider = "DataProvider" )
	void POSTCoverPhotos(String id  ) {
		
		
	RestAssured.baseURI="https://fakerestapi.azurewebsites.net/api/CoverPhotos";
		
	RequestSpecification httpRequest=RestAssured.given();
	
	JSONObject requestparms=new JSONObject();
	
	requestparms.put("ID", id);
	
	
	
	httpRequest.header("content-type","application/json");
	httpRequest.body(requestparms.toString("ID", id));
	
	Response response=httpRequest.request(Method.DELETE);
	
	
	
	Object responseBody=response.getBody().asString();
	
	
	System.out.println("Response Body :-"+responseBody);
	
	Assert.assertEquals(responseBody.equals(id),true);
	
	
	
	int statusCode=response.getStatusCode();
	Assert.assertEquals(statusCode,200);
	
	}
	

	
	@DataProvider(name="DataProvider")
	
	public Object [][] getEmpData() throws IOException{
		
		String path=System.getProperty("user.dir")+"/src/DELETE-Test-Data.xlsx";
		
		int rownum=XLS.getRowCount(path, "sheet1");
	int colcount=	XLS.getCellCount(path, "sheet1", 1);
			
		
	Object coverData[][]= new String[rownum][colcount];
		
		for(int i=1;i<=rownum;i++) {
			
			for(int j=0;j<=colcount;j++) {
				
				coverData[i-1][j]=XLS.getCellData(path, "sheet1", i, j);
				
			}
			
			
		}
		
		return(coverData);
	
	}}
